---
title: Toiles
date: 2012-01-10
---

Une Toile pas à pas, voici les photos que j'ai prises à la fin de chaque séance de peinture pour voir l'évolution de mon travail sur cette toile.

Dans un premier temps le dessin.

[![toile1](https://lh4.googleusercontent.com/-yXaOPxbNAT0/UaO6Zn3NquI/AAAAAAAAGJs/Yb_OaqgLGGU/2011-11-20+14.55.07.jpg)](https://lh4.googleusercontent.com/-yXaOPxbNAT0/UaO6Zn3NquI/AAAAAAAAGJs/Yb_OaqgLGGU/2011-11-20+14.55.07.jpg)

Première couche de peinture sur la toile pour faire ressortir la carrosserie de la voiture.&

[![toile2](https://lh3.googleusercontent.com/-4XePuAGEcNE/UaO6ZnOSLxI/AAAAAAAAGJw/4obnDyATY1c/2011-11-20+20.52.13.jpg)](https://lh3.googleusercontent.com/-4XePuAGEcNE/UaO6ZnOSLxI/AAAAAAAAGJw/4obnDyATY1c/2011-11-20+20.52.13.jpg)

le fait de peindre le second plan, va faire ressortir la voiture au premier plan.

[![toile3](https://lh5.googleusercontent.com/-EbCAuzM8wbE/UaO6aMVohlI/AAAAAAAAGJ4/fAG60mBUal0/2011-11-22+22.56.48.jpg)](https://lh5.googleusercontent.com/-EbCAuzM8wbE/UaO6aMVohlI/AAAAAAAAGJ4/fAG60mBUal0/2011-11-22+22.56.48.jpg)


[![toile4](https://lh3.googleusercontent.com/-cC5Yt963vQI/UaO6avVoFbI/AAAAAAAAGKA/fx0rODq5gdk/2011-11-29+23.10.53.jpg)](https://lh3.googleusercontent.com/-cC5Yt963vQI/UaO6avVoFbI/AAAAAAAAGKA/fx0rODq5gdk/2011-11-29+23.10.53.jpg)

Ajout du reflet du pare brise, et de chromes de phares

[![toile4](https://lh5.googleusercontent.com/-Y5GpICuw3Iw/UaO6bPkKWvI/AAAAAAAAGKI/fdsKxdWnw6E/2011-12-02+23.08.52.jpg)](https://lh5.googleusercontent.com/-Y5GpICuw3Iw/UaO6bPkKWvI/AAAAAAAAGKI/fdsKxdWnw6E/2011-12-02+23.08.52.jpg)

Un peu plus de détails pour l'immeuble au second plan

[![toile5](https://lh4.googleusercontent.com/-PJ5vXheEdCU/UaO6buvt2MI/AAAAAAAAGKM/xq6NGTbhWqs/2011-12-06+22.21.28.jpg)](https://lh4.googleusercontent.com/-PJ5vXheEdCU/UaO6buvt2MI/AAAAAAAAGKM/xq6NGTbhWqs/2011-12-06+22.21.28.jpg)
