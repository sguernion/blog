---
title: Mes premiers pas avec le Raspberry
date: 2013-03-07
---

[![rpi](https://lh6.googleusercontent.com/-rhfs2Nghfc4/UaOruB-XrkI/AAAAAAAAGI8/SLfbECdgIaY/DSCN1138.JPG)](https://lh6.googleusercontent.com/-rhfs2Nghfc4/UaOruB-XrkI/AAAAAAAAGI8/SLfbECdgIaY/DSCN1138.JPG)

Il y a peu j'ai découvert le [Raspberry](http://www.raspberrypi.org/) lors d'une conférence
[Hardware](http://www.breizhjug.org/2012/12/hard-is-back.html) au BreizhJug qui présentait quelques exemples d'application du Raspberry ou quelques autres cartes (Arduino, Beaglebone).

Pour ceux que ça intéresse le BreizhJug organise prochainement une conférence dans lequel il sera question de l'utilisation du Raspberry.

Allez sur le site du [BreizhJug](http://www.breizhjug.org/) pour plus d'informations.

Ayant quelques idées d'utilisations de celle-ci et vu sont coût je me suis décidé à en commander une.

Premier cas d'utilisation, en faire un "Media center". Dès la réception de la Raspberry, installation de [RaspBmc](http://www.raspbmc.com/wiki/user/) sur la carte SD. Branchement de la carte sur la télé, tout fonctionne du premier coup, même la télécommande de la télé est reconnue (grâce a la technologie
[HDMI](http://fr.wikipedia.org/wiki/High-Definition_Multimedia_Interface)
CEC).

Je décide de brancher un disque dur 2.5" auto-alimenté par usb, et là, problème cela fait rebouter la Raspberry.

La carte ne semble pas suffisamment puissante pour alimenter les périphériques usb.

La solution est passée par hub usb alimenté (D-Link DUB-H7 - 7 ports).

[![rpi2](https://lh6.googleusercontent.com/-In-imZa7RqM/UaOre3T0DuI/AAAAAAAAGIk/OSdGioDjewo/DSCN1131.JPG)](https://lh6.googleusercontent.com/-In-imZa7RqM/UaOre3T0DuI/AAAAAAAAGIk/OSdGioDjewo/DSCN1131.JPG)

En parcourant quelques blogs j'ai vu qu'il était possible d'exploiter les télécommandes de la WII.

Cela a attiré ma curiosité, j'ai donc décidé de commander en plus du hub une petite clé bluetooth. Sans doute prochainement un nouveau billet pour vous raconter ce que j'aurai pu faire avec la télécommande de la WII.

En attendant voici l'adresse de mon [repository](https://github.com/sguernion/myPI) sur github.
