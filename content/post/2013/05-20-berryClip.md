---
title: Test des Gpio avec la carte BerryClip
date: 2013-05-20
---

[![berryClip](https://lh4.googleusercontent.com/-_ec2K9G7Fn4/UaOrv-KWeiI/AAAAAAAAGJI/eYUjctErYU0/w913-h685-no/DSCN1271.JPG)](https://lh4.googleusercontent.com/-_ec2K9G7Fn4/UaOrv-KWeiI/AAAAAAAAGJI/eYUjctErYU0/w913-h685-no/DSCN1271.JPG)

un second article sur le [Raspberry](http://www.raspberrypi.org/) pour parler cette fois ci des Gpio.

Ce sont des entrées/sorties qui permettent d'intéragir avec l'extérieur (suivre le lien pour plus [info](https://www.modmypi.com/blog/raspberry-pi-gpio-en-franais">info</a>) ).

Par exemple commander un relais pour allumer une lampe ou exploiter des capteurs (distance, présence, température....)
