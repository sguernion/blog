---
title: Polymer
date: 2015-03-22
---


Je vais vous présenter une exemple de mise en place de [Polymer](https://www.polymer-project.org) sur un blog construit avec [jbake](http://jbake.org/).

## Présentation

[Polymer](https://www.polymer-project.org) est une library de Google vous permettant de créer des [web-componants](http://webcomponents.org/) fonctionnant sur tous les navigateurs.

Pour ceux qui ne connaissent pas les web-componants, les web-componants permettent la création et la réutilisation de ses propre balises html.

La spécification Web Components en cours de standardisation au sein du [W3C](http://www.journaldunet.com/w3c/).

Pour plus d’information, je vous laisse regarder la [présentation](https://www.youtube.com/watch?v=7ObA5u6SxiA) faite au [BreizhJug](http://www.breizhjug.org/) par [Horacio](https://twitter.com/LostInBrittany).


## Installation de Polymer

La façon la plus simple pour l’installation des Polymer, c’est d’utiliser [bower](http://bower.io/).

Pour ceux qui n’auraient pas installer bower, c’est par [ici](http://bower.io/#install-bower).

Si vous n’avez pas encore initialisé bower sur votre projet, vous pouvez le faire avec la commande :

```javascript
bower init
```

Pour modifier l’emplacement ou seront installer les packages de bower, il faut ajouter la ligne suivante dans le ficher de configuration .bowerrc :

```javascript
{
 "directory": "src/jbake/assets/components/"
}
```


Maintenant pour l’installation des polymer, il suffit de lancer la commande :

```javascript
bower install --save Polymer/polymer
```

Vous pouvez voir que dans votre configuration bower une dépendance a été ajoutée.

```javascript
"dependencies": {
    "polymer": "Polymer/polymer#~0.5.5"
  }
```

## Création d’un composant Polymer

On crée un fichier myComponents.html qui va contenir l’ensemble de nos composants. On peut également mettre chacun des composants dans son propre fichier html.

```javascript
<polymer-element name="blog-tag" attributes="link tag nb">
  <template>
	<style></style>
	<li><a href="{{link}}">{{tag}}</a>({{nb}})</li>
  </template>
  <script>
    Polymer();
  </script>
</polymer-element>
```

le style de composant peut être définit à l’intérieur des balises “<code>&lt;style&gt;&lt;/style&gt;</code>” .
Ce style sera appliqué seulement au template et ne sera pas propagé sur le reste du code html.


## Utilisation du composant

Maintenant pour utiliser votre composant Polymer, il faut ajouter les bon imports dans les balises “<code>&lt;head&gt;&lt;/head&gt;</code>”

```javascript
 	<!-- Polymer -->
	 <script src="components/webcomponentsjs/webcomponents.min.js"></script>
	 <link rel="import" href="components/myComponents.html"/>
```

Il suffit maintenant d’y ajouter notre nouveau composant la ou on souhaite l’afficher.

```javascript
<blog-tag link="tags/polymer.html" tag="Polymer" nb="1" ></blog-tag>
```

Vous pouvez retrouver l’exemple dans les [sources](https://github.com/sguernion/blog) du blog. Dans un prochain billet, une utilisation plus poussée des Polymer.</p>
